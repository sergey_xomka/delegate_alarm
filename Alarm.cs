﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ConsoleApp24_7_12_8_AlarmDelegate
{
    class Alarm
    {
        //номер звенящего будильника
        public static int numberAlarm;
        //время будильника
        public DateTime dateTime;
        //громкость
        private int volume;
        //всего будильников
        public static int alarmCount;


        // dateTime - время будильника
        // volume - громкость будильника
        public Alarm(DateTime dateTime, int volume)
        {
            this.dateTime = dateTime;
            this.volume = volume;
            alarmCount++;
        }

        // сравнение будильников
        public static bool operator <=(Alarm m1, Alarm m2) => m1.dateTime <= m2.dateTime;
        public static bool operator >=(Alarm m1, Alarm m2) => m1.dateTime >= m2.dateTime;
        
        // звонок будильника для Person
        public void Bzzz(Person p)
        {
            //Точность сравнения до секунд текущего дня
            if (dateTime.Hour != DateTime.Now.Hour
                || dateTime.Minute != DateTime.Now.Minute
                || dateTime.Second != DateTime.Now.Second) return;

            numberAlarm++;
            // Сонливость уменьшается на 0.1 часть громкости
            p.Drowsinnes -= volume / 10;
            if (p.Drowsinnes > 30)
            {
                Console.WriteLine($"{String.Empty,10}Alarm {numberAlarm}/{alarmCount} BZZZZZ. {p.Name} not wake up");
            }
            else
            {
                Console.WriteLine($"Alarm {numberAlarm}/{alarmCount} BZZZZZ. {p.Name} wake up");
                p.Sleep = false;
            }
        }
        public override string ToString() => $"Будильник {numberAlarm}: {dateTime}";
    }
}
