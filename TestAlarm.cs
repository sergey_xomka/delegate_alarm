﻿using System;
using System.Timers;

namespace ConsoleApp24_7_12_8_AlarmDelegate
{
    class TestAlarm
    {
        private Random random = new Random();
        private Del alarms = Console.WriteLine;
        private  Person person = new Person("Sarah Connor", 30);
        private Timer t = new Timer(1000);
        
        public void Test1()
        {
            for (int i = 0; i < 10; i++)
            {
                alarms += (new Alarm(DateTime.Now.AddSeconds(random.Next(3,30)), random.Next(100)))
                    .Bzzz;
            }

            t.Elapsed += TestElapsed;
            t.Start();
            Console.ReadLine();
        }

        private void TestElapsed(object sender, ElapsedEventArgs e)
        {
            // одиночный запуск
            alarms(person);
            if (!person.Sleep) t.Stop();

            if (Alarm.numberAlarm == Alarm.alarmCount)
            {
                t.Stop();
                Console.WriteLine($"{person} - GAME OVER");
            }
        }
    }
}