﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24_7_12_8_AlarmDelegate
{
    class Person
    {   
        static Random random = new Random();
        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
            this.Drowsinnes = random.Next(50,100);
            this.Sleep = true;
        }
        // сонливость
        public int Drowsinnes { get; set; }
        // спит?
        public bool Sleep { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        //спит или не спит
        public override string ToString()
        {
            string s = Sleep ? "sleep" : "not sleep";
            return $"{DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second:00}, Person {Name} {s}";
        }
    }
}
